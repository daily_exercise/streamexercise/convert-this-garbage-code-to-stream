/**
 * 
    public static Collection<String> mapToUppercase(String... names) {
    Collection<String> uppercaseNames = new ArrayList<>();
    for(String name : names) {
        uppercaseNames.add(name.toUpperCase());
    }
    return uppercaseNames;
}

Convert this garbage code to Stream

*/

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task1 {
    public static Collection<String> mapToUppercase(String... names) {
        return Stream.of(names)
                .map(m -> m.toUpperCase())
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        Task1.mapToUppercase("hi", "HeLLo", "KILL").forEach(System.out::println);
    }
}
